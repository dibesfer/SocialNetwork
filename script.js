
//import { createClient } from '@supabase/supabase-js'
const supabaseUrl = 'https://qugihsopwjemzakhrbvw.supabase.co'
const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InF1Z2loc29wd2plbXpha2hyYnZ3Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTg0OTQxMDksImV4cCI6MjAxNDA3MDEwOX0.1q5fBic1cjueaiP2-p6W19C68ye8FTPLFne2a-fKwZ8'
const database = supabase.createClient(supabaseUrl, supabaseKey)

var globaluser;
var globalusertable;
var updateLastTimeInterval;
var demo = document.getElementById("demo");

var randomPlaceholders = [
    "Write something beautiful! 🦋💜",
    "Write something insightful! 🤔💭",
    "Write something meaningful! 💚🙏",
    "How's been your day? ⛅",
    "What I miss the most is... 🧡",
    "What I like the most is ... ❤️",
    "I gotta say this! 💙"
]

//demo.innerText = 'Supabase Instance: ' + JSON.stringify(database)


// Create a function to handle inserts
const handleInserts = (payload) => {
    console.log('Change received!', payload)
    getResources("feed", "*")
}

// Listen to inserts
database
    .channel('feed')
    .on('postgres_changes', { event: 'INSERT', schema: 'public', table: 'feed' }, handleInserts)
    .subscribe()

async function getResources(tableName, rowName) {
    const res = await database.from(tableName).select(rowName)
    if (res.data[0] != undefined) {
        feed.innerHTML = "<p>" + res.data[0].created_at.split("T")[0] + "</p>"

        res.data.forEach(element => {
            feed.innerHTML += "<i>" + element.created_at.split("T")[1].split(".")[0] + "</i> <b>" + element.author + "</b>: " + element.message + "</br>"
        });
    }
    feed.scrollTop = feed.scrollHeight
}


getResources("feed", "*")



async function getUserTable(email) {
    const res = await database.from("users").select("*").eq('email', email)
    globalusertable = res.data[0]
    showUsername.innerText = globalusertable.username
    if (!globalusertable.profilepicture) showProfilepicture.innerHTML = "<img class='userPfp' src=' https://icon-library.com/images/default-user-icon/default-user-icon-3.jpg '>"
    else showProfilepicture.innerHTML = "<img class='userPfp' src=' " + globalusertable.profilepicture + " '>"
    //showLastTime.innerText = globalusertable.last_time_connected
    updateLastTime(email)

}


async function createNewUser() {
    let emailInput = getid("emailInput")
    let passwordInput = getid("passwordInput")
    let { data, error } = await database.auth.signUp({
        email: emailInput.value,
        password: passwordInput.value
    })
    console.log(data)
    console.log(error)
    location.reload();
    insertResources("users", emailInput.value)

}


async function insertResources(tableName, param1) {
    const { data, error } = await database
        .from(tableName)
        .insert([
            {
                email: param1,
                username: param1.split("@")[0]
            },
        ])
        .select()
}

async function signIn() {
    let emailInput = getid("emailInput")
    let passwordInput = getid("passwordInput")
    let { data, error } = await database.auth.signInWithPassword({
        email: emailInput.value,
        password: passwordInput.value
    })
    console.log(data)
    location.reload();
}

async function getUser() {
    const { data: { user } } = await database.auth.getUser()
    if (user) {
        console.log("user available")
        ifUser.classList.toggle("hidden")
        getUserTable(user.email)
        showEmail.innerText = user.email
        //showUsername.innerText = globalusertable.username
        updateLastTimeInterval = setInterval(() => {
            updateLastTime(globalusertable.email)
        }
            , 10000);

        messageTextarea.placeholder = randomPlaceholders[randomInt(0, randomPlaceholders.length - 1)]

    }
    else {
        console.log("user unavailable")
        ifNotUser.classList.toggle("hidden")
        //globaluser = user

    }

}
//getUser()
getUser()
async function logOut() {
    let { error } = await database.auth.signOut()
    location.reload();

}

async function sendFeed() {


    if (messageTextarea.value != "") {
        const { data, error } = await database
            .from("feed")
            .insert([
                {
                    author: globalusertable.username,
                    message: messageTextarea.value
                },
            ])
            .select()
        //location.reload()
        //console.log(data)

        messageTextarea.placeholder = randomPlaceholders[randomInt(0, randomPlaceholders.length - 1)]
        messageTextarea.value = ""

    }
    else 
        messageTextarea.placeholder = "You can't send an 'empty' message!"
}

async function updateLastTime(email) {

    const d = new Date();
    let text = d.toISOString();
    //console.log(`Time elapsed: ${Date.now() - d} ms`)
    console.log(text)
    const { data, error } = await database
        .from('users')
        .update({ last_time_connected: text })
        .eq('email', email)
        .select()
    showLastTime.innerText = text

}

function toggleEditMode(theThis, element) {
    if (theThis.innerHTML == "✏️") {
        let placeholder = element.innerHTML
        element.innerHTML = "<input style='background-color:gray' type='text' placeholder='" + placeholder + "'  >"
        theThis.innerHTML = "✔️✗"
    }
    else {
        theThis.innerHTML = "✏️"
        element.innerHTML = globalusertable.username
    }

}


/* 
how many seconds a date from now
var d = new Date("2024-02-09T19:10:28.25+00:00")
var now = new Date()
var nowISO = now.toISOString()
var difference = new Date(now-d)

document.write(d.toISOString() + "<br>")
document.write(nowISO+ "<br>")
document.write(difference.getTime()/1000 )
document.write() */